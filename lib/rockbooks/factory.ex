defmodule Rockbooks.Factory do
  use ExMachina.Ecto, repo: Rockbooks.Repo

  def user_factory do
    %Rockbooks.User{
      name: "Muhammad Ali",
      email: sequence(:email, &"email-#{&1}@example.com"),
      encrypted_password: Comeonin.Bcrypt.hashpwsalt("secret_password")
    }
  end
end
