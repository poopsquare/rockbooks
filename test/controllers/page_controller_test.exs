defmodule Rockbooks.PageControllerTest do
  use Rockbooks.ConnCase
  test "renders index", %{conn: conn} do
    conn = get conn, page_path(conn, :index)
    assert html_response(conn, 200) =~ "Rockbooks!"
  end
end
