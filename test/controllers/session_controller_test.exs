defmodule Rockbooks.SessionControllerTest do
  use Rockbooks.ConnCase

  alias Rockbooks.User

  @invalid_attrs %{email: "erlich@aviato.com", password: "wrong_password"}

  test "when not authenticated renders login", %{conn: conn} do
    conn = get conn, session_path(conn, :new)
    assert html_response(conn, 200) =~ "Login"
  end

  test "login with correct username password combo", %{conn: conn} do
    user = insert(:user)
    conn = post conn, session_path(conn, :create), session: %{email: user.email, password: "secret_password"}
    assert redirected_to(conn) == page_path(conn, :index)
    assert get_session(conn, :current_user) == user.id
  end

  test "do not login with incorrect username password combo", %{conn: conn} do
    user = insert(:user)
    conn = post conn, session_path(conn, :create), session: @invalid_attrs
    assert html_response(conn, 200) =~ "Wrong email or password"
    refute get_session(conn, :current_user) == user.id
  end

  test "deletes session when logging out", %{conn: conn} do
    user = insert(:user)
    conn = post conn, session_path(conn, :create), session: %{email: user.email, password: "secret_password"}
    assert get_session(conn, :current_user) == user.id
    conn = delete conn, session_path(conn, :delete)
    assert redirected_to(conn) == page_path(conn, :index)
    refute get_session(conn, :current_user) == user.id
  end
end
