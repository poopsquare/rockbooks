defmodule Rockbooks.RegistrationControllerTest do
  use Rockbooks.ConnCase

  alias Rockbooks.User

  @valid_attrs %{email: "erlich@aviato.com", password: "secret_password", name: "Erlich Bachmann"}
  @invalid_attrs %{}

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, registration_path(conn, :new)
    assert html_response(conn, 200) =~ "Create an account"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, registration_path(conn, :create), user: @valid_attrs
    assert redirected_to(conn) == page_path(conn, :index)
    assert Repo.get_by(User, Map.delete(@valid_attrs, :password))
  end

  test "does not create resource and re-renders form when data is not valid", %{conn: conn} do
    conn = post conn, registration_path(conn, :create), user: @invalid_attrs
    assert html_response(conn, 200) =~ "Create an account"
    refute Repo.get_by(User, Map.delete(@invalid_attrs, :password))
  end
end
