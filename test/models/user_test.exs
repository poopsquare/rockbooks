defmodule Rockbooks.UserTest do
  use Rockbooks.ModelCase

  alias Rockbooks.User

  @valid_attrs   %{email: "email@test.com", password: "secret_password", name: "John Doe"}
  @invalid_email %{email: "invalid_email", password: "secret_password", name: "John Doe"}
  @short_passwd  %{email: "email@test.com", password: "123", name: "John Doe"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "changeset with invalid email" do
    changeset = User.changeset(%User{}, @invalid_email)
    refute changeset.valid?
  end

  test "changeset with invalid password" do
    changeset = User.changeset(%User{}, @short_passwd)
    refute changeset.valid?
  end
end
