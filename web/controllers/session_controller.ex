defmodule Rockbooks.SessionController do
  use Rockbooks.Web, :controller

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"session" => session_params}) do
    case Rockbooks.Session.login(session_params, Rockbooks.Repo) do
      {:ok, user} ->
        conn
        |> put_session(:current_user, user.id)
        |> put_flash(:info, gettext("Logged in"))
        |> redirect(to: "/")
      :error ->
        conn
        |> put_flash(:info, gettext("Wrong email or password"))
        |> render("new.html")
    end
  end


  def delete(conn, _) do
    conn
    |> delete_session(:current_user)
    |> put_flash(:info, gettext("Logged out"))
    |> redirect(to: "/")
  end
end
