defmodule Rockbooks.PageController do
  use Rockbooks.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
