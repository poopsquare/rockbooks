use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rockbooks, Rockbooks.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :rockbooks, Rockbooks.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  ownership_timeout: 1000000,
  password: "postgres",
  database: "rockbooks_test",
  hostname: System.get_env("ROCKBOOKS_DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
